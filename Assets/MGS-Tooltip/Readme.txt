==========================================================================
  Copyright (C), 2017-2018, Mogoson tech. Co., Ltd.
  Name: MGS-Tooltip
  Author: Mogoson   Version: 1.0   Date: 6/13/2017
==========================================================================
  [Summeray]
    This package can be used to make tooltip in Unity3D scene.
--------------------------------------------------------------------------
  [Environment]
    Package applies to Unity3D 5.0, .Net Framework 3.0 or above version.
--------------------------------------------------------------------------
  [Usage]
    Find the demos in the path "MGS-Tooltip/Scenes".
    Understand the usages of component scripts in the demos.
    Use the compnent scripts in your project.
--------------------------------------------------------------------------
  [Contact]
    If you have any questions, feel free to contact me at mogoson@qq.com.
--------------------------------------------------------------------------