﻿#MGS-Tooltip

##概述
Unity3D 制作场景工具（物体）提示UI（UGUI）插件包

##需求
在Unity3D场景中，当鼠标指针移动到某个工具（物体）上时，UI显示一些关于工具（物体）的简要信息。

##方案
1. UGUI制作提示UI，适应不同长度，格式的提示信息。
1. 编写UI控制脚本，负责更新显示文本，适应鼠标指针位置，适应屏幕边界。
1. 编写提示UI触发器脚本，触发显示/关闭。

##实现
- TooltipUI.cs：控制提示UI。
- TooltipTrigger.cs：触发提示UI。

##案例
- “MGS-Tooltip/Prefabs”目录下存放有UGUI制作的提示UI预制体。
- “MGS-Tooltip/Scenes”目录下存放有演示案例，供读者参考。
